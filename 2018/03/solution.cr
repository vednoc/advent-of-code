# 03

input = File.read("#{__DIR__}/input.txt")

dim = 1000
fabric = Array.new(dim) { Array.new(dim, 0) }
input.each_line do |i|
  i.each_line do |j|
    a = j.split(" @ ")[1].split(": ")
    x = a[0].split(",")[0].to_i
    y = a[0].split(",")[1].to_i
    w = a[1].split("x")[0].to_i
    h = a[1].split("x")[1].to_i
    (x..x + w - 1).each do |x|
      (y..y + h - 1).each do |y|
        fabric[x][y] += 1
      end
    end
  end
end

part_one = fabric.flatten.count { |i| i > 1 }
puts "Solution to the first part is: #{part_one}"
